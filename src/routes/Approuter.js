import React, { Component } from 'react'
import {  Route, Switch } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from "history";
import Notification from '../feature/notification'
import { Dashboard, Home, Profile } from '../pages'
import ProtectedRoute from './ProtectedRoute'
import { AuthProvider } from '../feature/auth/'

export const history = createBrowserHistory();

// AppRouter component is used to declare the routes through the application.
// All the pages are imported here and assigned to either a PublicRoute or a PrivateRoute
// You can create new types of routes as required and set them up here
class Approuter extends Component {
  render () {
    return (
      <ConnectedRouter history={history}>
        <AuthProvider>
        <div className='App'>
          <Notification />
          <Switch>
            <Route path='/' exact component={Home} />
            <ProtectedRoute path='/dashboard' component={Dashboard} />
            <ProtectedRoute path='/profile' component={Profile} />
          </Switch>
        </div>
        </AuthProvider>
      </ConnectedRouter>
    )
  }
}

Approuter.propTypes = {}

export default Approuter
