// leave off @2x/@3x
const Images = {
    profile: require('./images/icons/profile.svg'),
    user: require('./images/icons/user.svg'),
    dashboard: require('./images/icons/dashboard.svg'),
    home: require('./images/icons/home.svg'),
    notification: require('./images/icons/notification.svg'),
    logo: require('./images/icons/logo.svg'),
  }
  
  export default Images
  