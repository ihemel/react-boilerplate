import React, { Component } from 'react'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { theme } from 'super-comp'
import AppRouter from './routes/Approuter';

class App extends Component {
  render () {
    return (
      <MuiThemeProvider theme={theme}>
        <AppRouter/>
      </MuiThemeProvider>
    )
  }
}

export default App
