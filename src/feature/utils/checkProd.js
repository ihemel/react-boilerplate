export const checkProd = () => {
    const devCheck = new RegExp("([a-z0-9]+.)*[a-z0-9]+.com+")
    return devCheck.test(window.location.hostname)
  }
  