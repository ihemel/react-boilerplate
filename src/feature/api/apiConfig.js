let baseUrl = 'https://api.backend.com/'
if (process.env.NODE_ENV !== 'production') {
  baseUrl = 'https://dev.backend.com/'
}

export { baseUrl }

