import { baseUrl } from './apiConfig'

export const api = (path, method, params, token) => async () => {
  const apiUrl = baseUrl + path
  const options = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: method
  }

  if (params) {
    options.body = JSON.stringify(params)
  }

  if (token) {
    options.headers.Authorization = token
  }

  try {
    const res = await fetch(apiUrl, options);
    const status = res.status
    const data = await res.json();
    return {data, status};
  }
  catch (error) {
    return {error, status: 500};
  }
}


export const apiBlob = (path, token) => async () => {
  const apiUrl = baseUrl + path
  const options = {
    headers: {
      'Accept': 'application/octet-stream',
      'Content-Type': 'application/octet-stream'
    },
    method: "GET"
  }

  if (token) {
    options.headers.Authorization = token
  }

  try {
    const res = await fetch(apiUrl, options);
    const status = res.status
    const data = await res.blob();
    return {data, status};
  }
  catch (error) {
    return {error, status: 500};
  }
}



