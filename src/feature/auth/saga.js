import { put } from 'redux-saga/effects'
import * as act from './Actions'
import { enqueueSnackbar } from '../notification/Actions'
import { push } from 'connected-react-router';

export function * LoginSaga (action) {
  try {
    const response = action.payload
    if (response.id) {

      yield put(act.loginSuccess(response))

      yield put(
        enqueueSnackbar({
          message: 'Success!',
          options: {
            variant: 'success',
            autoHideDuration: 1500
          }
        })
      )

      yield put(push('/dashboard'))
    } else {
      yield put(act.loginFail(response))

      yield put(
        enqueueSnackbar({
          message: 'Wrong Credentials!',
          options: {
            variant: 'warning',
            autoHideDuration: 1500
          }
        })
      )
    }
  } catch (error) {
    yield put(act.loginFail(error))
  }
}



export function * LogoutSaga (action) {
    try {
      const response = action.payload
      if (response.id) {
        yield put(act.logoutSuccess(response))
  
        // history.push()
      } else {
        yield put(act.logoutFail(response))
  
        yield put(
          enqueueSnackbar({
            message: 'Wrong Facebook Credentials!',
            options: {
              variant: 'warning',
              autoHideDuration: 1500
            }
          })
        )
      }
    } catch (error) {
      yield put(act.logoutFail(error))
    }
  }
  