import * as types from './Types'

export const loginRequest = payload => {
  return {
    type: types.LOGIN_REQUEST,
    payload
  }
}

export const loginSuccess = payload => {
  return {
    type: types.LOGIN_SUCCESS,
    payload
  }
}

export const loginFail = payload => {
  return {
    type: types.LOGIN_FAIL,
    payload
  }
}


export const logoutRequest = payload => {
    return {
      type: types.LOGOUT_REQUEST,
      payload
    }
  }
  
  export const logoutSuccess = payload => {
    return {
      type: types.LOGOUT_SUCCESS,
      payload
    }
  }
  
  export const logoutFail = payload => {
    return {
      type: types.LOGOUT_FAIL,
      payload
    }
  }
  