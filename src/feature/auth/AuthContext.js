import React from 'react'
import { connect } from 'react-redux'
import { loginRequest, logoutRequest } from './Actions'

const AuthContext = React.createContext()

class AuthProvider extends React.Component {

  login = () => {
    this.props.login({id : 1})
  }

  logout = () => {
    this.props.logout({id: 1})
  }

  render () {
    return (
      <AuthContext.Provider
        value={{
          isAuth: this.props.Auth.auth_status,
          login: this.login,
          logout: this.logout
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    )
  }
}

const mapStateToProps = ({ Auth }) => ({
  Auth: Auth,
})

const mapDispatchToProps = dispatch => ({
  login: (payload) => dispatch(loginRequest(payload)),
  logout: (payload) => dispatch(logoutRequest(payload)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthProvider)

export const AuthConsumer = AuthContext.Consumer
