import createReducer from '../lib/createReducer'
import * as types from './Types'

const initialState = {
}

export const Auth = createReducer(initialState, {
  [types.LOGIN_REQUEST]: (state, action) => ({
    ...state
  }),

  [types.LOGIN_SUCCESS]: (state, action) => ({
    ...state,
    auth_status: true
  }),

  [types.LOGIN_FAIL]: (state, action) => ({
    ...state,
    auth_status: false,
    has_error: true
  }),

  [types.LOGOUT_REQUEST]: (state, action) => ({
    ...state
  }),

  [types.LOGOUT_SUCCESS]: (state, action) => ({
    ...state,
    auth_status: false
  }),

  [types.LOGOUT_FAIL]: (state, action) => ({
    ...state,
    auth_status:false,
    has_error: true
  })
})
