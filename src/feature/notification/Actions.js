import * as types from './Types'

export const enqueueSnackbar = notification => {
  return {
    type: types.ENQUEUE_SNACKBAR,
    notification: {
        key: new Date().getTime() + Math.random(),
        ...notification,
    },
  }
}

export const removeSnackbar = key => {
  return {
    type: types.REMOVE_SNACKBAR,
    key,
  }
}

