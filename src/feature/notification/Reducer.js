import createReducer from '../lib/createReducer'
import * as types from './Types'

const initialState = {
  notifications: [],
}

export const notificationReducer = createReducer(initialState, {

  [types.ENQUEUE_SNACKBAR]: (state, action) => {
    return {
      ...state,
      notifications: [
        ...state.notifications,
        {
          ...action.notification,
        },
      ],
    }
  },
  [types.REMOVE_SNACKBAR]: (state, action) => {
    return {
      ...state,
      notifications: state.notifications.filter(
        notification => notification.key !== action.key,
      ),
    }
  },
})

