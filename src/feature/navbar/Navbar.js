import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Nav, Logo, User, CircleBtn } from 'super-comp'
import { AuthConsumer } from '../auth'
import { Images } from "../../assets/";

import './Navbar.css'

const styles = {
  root: {
    height: '4.2rem',
    padding: '.3rem .5rem',
    '@media(min-width: 37.5em)': {
      padding: '.4rem 1rem'
    },
    '@media(min-width: 76.25em)': {
      width: '62.5rem',
      alignSelf: 'center',
      padding: '.2rem 0rem'
    }
  },
  user: {
    width: '3.5rem',
    height: '3.5rem'
  },
  notification: {
    minWidth: 0,
    width: '2.5rem',
    height: '2.5rem'
  }
}

const Navbar = props => {
  const { classes } = props
  return (
    <div className='Navbar'>
      <Nav className={classes.root}>
        <Logo style={{ width: '3.6rem', height: '3.6rem' }} src={Images.logo} alt='Logo' />

        <div>
          <h4 className='sh-store-name'>Boilerplate</h4>
        </div>
        <AuthConsumer>
          {({ isAuth, login, logout }) => (
            <div className='user-box'>
              <CircleBtn
                className={classes.notification}
                src={Images.notification}
                alt='notification'
              />
              <User className={classes.user} src={Images.user} alt='User' isAuth={isAuth} login={login} logout={logout} />
            </div>
          )}
        </AuthConsumer>
      </Nav>
    </div>
  )
}

export default withStyles(styles)(Navbar)
