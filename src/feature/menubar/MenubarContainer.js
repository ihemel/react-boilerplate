import React from 'react'
import { connect } from 'react-redux'
import Menubar from './Menubar'
import { Images } from "../../assets/";



class MenubarContainer extends React.PureComponent {

  state = {
      menuItems: [
          {
            title: "Home",
            link: "/",
            icon: `${Images.home}`
          },
          {
            title: 'Profile',
            link: '/profile',
            icon: `${Images.profile}`
          },
          {
            title: 'Dashboard',
            link: '/dashboard',
            icon: `${Images.dashboard}`
          }, 
      ]
  }

  render () {
    return <Menubar {...this.state} {...this.props} />
  }
}

const mapStateToProps = state => ({
    
})

export default connect(
  mapStateToProps,
  null
)(MenubarContainer)
