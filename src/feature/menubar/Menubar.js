import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Nav, MenuBtn } from 'super-comp'
import { Link } from 'react-router-dom'


import './MenuBar.css'

const styles = {
  appbarRoot: {
    width: '1000px',
    margin: '0 auto',
    position: 'relative',
    zIndex: '99'
  },
  btn: {
    border: 0,
    borderRadius: 0,
    width: '100%',
    height: '3.5rem',
    fontFamily: 'apercu bold',
    textTransform: 'capitalize',
    fontSize: '1.1rem',
    lineHeight: '1',

    '&:not(:last-child)': {
      borderRight: '1px solid var(--grey2-color)'
    },

    '&:hover': {
      transition: 'all .5s',
      backgroundColor: 'var(--primary-color)'
    }
  }
}

const Menubar = props => {
  const { classes, menuItems } = props

  let menus = []
  // eslint-disable-next-line array-callback-return
  menuItems.map(item => {
    menus.push(
    <MenuBtn
      key={item.title}
      component={Link}
      to={item.link}
      title={item.title}
      icon={item.icon}
      iconClass='menubar__icon'
      alt='menu'
      onClick={() => {
        console.log('clicked')
      }}
      />
    )
  })


  return (
    <Nav appbarclass={classes.appbarRoot}>
      {menus}
    </Nav>
  )
}

export default withStyles(styles)(Menubar)
