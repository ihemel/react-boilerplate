import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Popover from '@material-ui/core/Popover'
import classNames from 'classnames'

const styles = theme => ({
  button: {
    background: theme.palette.common.white,
    // boxShadow: `0 0 4px 0 ${theme.text.black}`,
    boxShadow: '0 0 4px 0 rgba(0, 0, 0, 0.14), 0 3px 3px 0 rgba(0, 0, 0, 0.12), 0 -2px 4px 0 rgba(0, 0, 0, 0.1)',
    // border: `1px solid ${theme.text.grey}`,
    border: 'none',
    textTransform: 'capitalize',
    fontFamily: 'Apercu Black',
    color: theme.text.primary,
    fontSize: '1rem',
    outline: 'none',
    borderRadius: '6px',
    minWidth: '0',
    padding: '.7rem 1.4rem',
    marginRight: '.2rem',

    '&:hover': {
      background: 'transparent',
      boxShadow: `0 0 4px 0 ${theme.text.black}`
    },
    '&:active': {
      background: 'transparent',
      boxShadow: `0 0 4px 0 ${theme.text.black}`
    },

    '@media(max-width: 21.25em )': {
      padding: '0.7rem .8rem',
    }
  },
  paper: {
    boxShadow: 'none',
    background: 'transparent'
  }
})

class PrimaryPopover extends React.Component {
  state = {
    anchorEl: null
  }

  handleClick = event => {
    this.setState({
      anchorEl: event.currentTarget
    })
  }

  handleClose = () => {
    this.setState({
      anchorEl: null
    })
  }

  render () {
    const {
      title,
      icon,
      alt,
      iconclass,
      iconstyle,
      classes,
      className,
      children
    } = this.props
    const { anchorEl } = this.state
    const open = Boolean(anchorEl)

    return (
      <div>
        <Button
          aria-owns={open ? 'simple-popper' : undefined}
          aria-haspopup='true'
          variant='contained'
          onClick={this.handleClick}
          className={classNames(classes.button, className)}
        >
          <img src={icon} alt={alt} style={iconstyle} className={iconclass} />
          {title}
        </Button>
        <Popover
          classes={{ paper: classes.paper }}
          id='simple-popper'
          open={open}
          anchorEl={anchorEl}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
        >
          {children}
        </Popover>
      </div>
    )
  }
}

PrimaryPopover.propTypes = {
  classes: PropTypes.object,
  title: PropTypes.string,
  icon: PropTypes.any,
  alt: PropTypes.string,
  iconStyle: PropTypes.object
}

export default withStyles(styles)(PrimaryPopover)
