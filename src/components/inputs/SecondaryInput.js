import React from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import { withStyles } from '@material-ui/core/styles'


const styles = {
  adornmentRoot: {
    background: 'red',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '0',
    margin: '0',
  },
  input: {
    padding: '0'
  }
}

const SecondaryInput = props => {
  const { classes, className, ...others } = props;

  return (
    // <TextField
    //   variant="outlined"
    //   className={className}
    //   {...others}
    // />
    <TextField
        variant="outlined"
        className={className}
        {...others}
        // InputProps={{
        //   startAdornment: 
        //   <InputAdornment 
        //   position="start" 
        //   classes = {{root: classes.adornmentRoot}}
        //   >
        //     $
        //   </InputAdornment>,
        //   root: classes.root
        // }}
    />
  );
};

SecondaryInput.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  InputProps: PropTypes.object
};

export default withStyles(styles)(SecondaryInput)
