import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'

import ReactDropzone from 'react-dropzone'

const styles = theme => ({
  root: {
    height: 'inherit',
    minHeight: 'inherit',
    borderRadius: '2px',
    borderStyle: 'none',
    backgroundColor: theme.text.grey
  }
})

const DragDropFileInput = props => {
  const { classes, handleChange, className, children, ...others } = props

  return (
    <ReactDropzone
      className={classNames(classes.root, className)}
      onDrop={handleChange}
      {...others}
    >
      {children}
    </ReactDropzone>
  )
}

DragDropFileInput.propTypes = {
  handleChange: PropTypes.func
}

export default withStyles(styles)(DragDropFileInput)
