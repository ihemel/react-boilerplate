import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'


const styles = {
  cssLabel: {
    '&$cssFocused': {
      color: "var(--grey-color)",
    },
  },
  cssUnderline: {
    '&:after': {
      borderBottomColor: "var(--grey-color)",
    },
  },
  cssOutlinedInput: {
    backgroundColor: '#fff',
    '&$cssFocused $notchedOutline': {
      borderColor: "var(--grey-color)",
      border: '1px solid'
    },
  },
}

const PrimaryInput = props => {
  const { classes, className, ...others } = props

  return (
    <TextField
      variant='outlined'
      className={className}
      InputLabelProps={{
        classes: {
          root: classes.cssLabel,
          focused: classes.cssFocused,
        },
      }}
      InputProps={{
        classes: {
          root: classes.cssOutlinedInput,
          focused: classes.cssFocused,
          notchedOutline: classes.notchedOutline,
        },
      }}
      {...others}
    />
  )
}

export default withStyles(styles)(PrimaryInput)


