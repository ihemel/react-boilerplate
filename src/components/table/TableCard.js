import React from 'react'
import { withStyles } from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    borderBottom: '1px solid var(--grey2-color)',
    justifyContent: 'center',
    padding: '0.5em 1.5em',
    position: 'relative'
  },
  dotted: {
    position: 'absolute',
    top: '5px',
    right: '5px',
    cursor: 'pointer'
  },
  dottedIcon: {
    color: 'var(--grey-color)'
  }
})

function TableCard (props) {
  const { classes, children, className, ...otherProps } = props
  return (
    <div className={classNames(classes.root, className)} {...otherProps}>
      {children}
      <span className={classes.dotted} onClick={() => console.log('Dotted')}>
        <MoreVertIcon className={classes.dottedIcon} />
      </span>
    </div>
  )
}

export default withStyles(styles)(TableCard)
