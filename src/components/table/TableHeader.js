import React from 'react'
import { withStyles } from '@material-ui/core'

const styles = theme => ({
  tableHeaderContainer: {
    display: 'flex',
    flexDirection: 'row',
    background: 'var(--yellow-color)',
    borderRadius: '4px 4px 0px 0px',
    // textAlign: 'center',
    borderBottom: '1px solid var(--grey2-color)',
    padding: '0.5em 1.5em'
  }
})

function TableHeader (props) {
  const { classes, children } = props
  return <div className={classes.tableHeaderContainer}>{children}</div>
}

export default withStyles(styles)(TableHeader)
