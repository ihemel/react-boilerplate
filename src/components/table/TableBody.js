import React from 'react'
import { withStyles } from '@material-ui/core'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    background: 'var(--white-color)'
  }
})

function TableBody (props) {
  const { classes, children, className, ...otherProps } = props
  return (
    <div className={classNames(classes.root, className)} {...otherProps}>
      {children}
    </div>
  )
}

export default withStyles(styles)(TableBody)
