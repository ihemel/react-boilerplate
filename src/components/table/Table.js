import React from 'react'
import { withStyles } from '@material-ui/core'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    background: 'var(--grey3-color)',
    border: '1px solid var(--grey2-color)',
    borderBottom: 'none',
    borderRadius: '5px 5px 0px 0px',
    boxSizing: 'border-box'
  }
})

function Table (props) {
  const { classes, className, ...otherProps } = props
  return (
    <div className={classNames(classes.root, className)} {...otherProps}>
      {props.children}
    </div>
  )
}

export default withStyles(styles)(Table)
