import React from 'react'
import { withStyles } from '@material-ui/core'
import classNames from 'classnames'

function TableText (props) {
  const { bold, primary } = props
  const styles = theme => ({
    tableText: {
      justifyContent: 'center',
      color: primary ? 'var(--primary-text-color)' : 'var(--grey-color)',
      fontWeight: bold ? 'bold' : 'normal',
      lineHeight: '1.25rem',
      ...props.style
    }
  })
  const Text = function (props) {
    const { classes, children, className } = props

    return (
      <p className={classNames(classes.tableText, className)}>{children}</p>
    )
  }
  const TextWithStyle = withStyles(styles)(Text)
  return <TextWithStyle>{props.children}</TextWithStyle>
}

export default TableText
