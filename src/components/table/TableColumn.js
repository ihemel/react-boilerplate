import React, { useState } from 'react'
import { withStyles } from '@material-ui/core'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp'

const styles = theme => ({
  tableColumn: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1
    // justifyContent: 'center'
  },
  tableColumnHeader: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    '&:hover': {
      cursor: 'pointer'
    }
  },
  arrowIcon: {
    fontSize: '1.375rem'
  }
})

function TableColumn (props) {
  const [sort, setSort] = useState(true)
  const { classes, children } = props
  const sortColumn = () => {
    setSort(!sort)
    props.onClick()
  }
  if (props.sortable) {
    return (
      <div className={classes.tableColumnHeader} onClick={sortColumn}>
        {children}
        {sort ? (
          <ArrowDropDownIcon className={classes.arrowIcon} />
        ) : (
          <ArrowDropUpIcon className={classes.arrowIcon} />
        )}
      </div>
    )
  } else {
    return <div className={classes.tableColumn}>{children}</div>
  }
}

export default withStyles(styles)(TableColumn)
