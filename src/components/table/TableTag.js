import React, { useState } from 'react'
import Tooltip from '@material-ui/core/Tooltip'
import { withStyles } from '@material-ui/core/styles'

function arrowGenerator (color) {
  return {
    '&[x-placement*="bottom"] $arrow': {
      top: 0,
      left: 0,
      marginTop: '-0.95em',
      width: '3em',
      height: '1em',
      '&::before': {
        borderWidth: '0 1em 1em 1em',
        borderColor: `transparent transparent ${color} transparent`
      }
    },
    '&[x-placement*="top"] $arrow': {
      bottom: 0,
      left: 0,
      marginBottom: '-0.95em',
      width: '3em',
      height: '1em',
      '&::before': {
        borderWidth: '1em 1em 0 1em',
        borderColor: `${color} transparent transparent transparent`
      }
    },
    '&[x-placement*="right"] $arrow': {
      left: 0,
      marginLeft: '-0.95em',
      height: '3em',
      width: '1em',
      '&::before': {
        borderWidth: '1em 1em 1em 0',
        borderColor: `transparent ${color} transparent transparent`
      }
    },
    '&[x-placement*="left"] $arrow': {
      right: 0,
      marginRight: '-0.95em',
      height: '3em',
      width: '1em',
      '&::before': {
        borderWidth: '1em 0 1em 1em',
        borderColor: `transparent transparent transparent ${color}`
      }
    }
  }
}

const styles = theme => ({
  arrowPopper: arrowGenerator('rgba(0, 0, 0, 0.8)'),
  tooltip: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)'
  },
  arrow: {
    position: 'absolute',
    fontSize: 6,
    width: '3em',
    height: '3em',
    '&::before': {
      content: '""',
      margin: 'auto',
      display: 'block',
      width: 0,
      height: 0,
      borderStyle: 'solid'
    }
  },
  text: {
    fontSize: '0.75rem',
    fontWeight: 'bold'
  },
  tag: {
    background: 'var(--primary-color)',
    padding: '0.1em 0.3em',
    border: '1px solid var(--grey-color)',
    boxSizing: 'border-box',
    borderRadius: '2px',
    alignSelf: 'start'
  }
})

function TableTag (props) {
  const [arrowRef, setArrowRef] = useState(null)

  const { classes, children, tooltip, done } = props
  return (
    <Tooltip
      title={
        <React.Fragment>
          {tooltip}
          <span className={classes.arrow} ref={setArrowRef} />
        </React.Fragment>
      }
      classes={{ popper: classes.arrowPopper, tooltip: classes.tooltip }}
      PopperProps={{
        popperOptions: {
          modifiers: {
            arrow: {
              enabled: Boolean(arrowRef),
              element: arrowRef
            }
          }
        }
      }}
    >
      <div
        className={classes.tag}
        style={
          done
            ? {
              backgroundColor: 'var(--blue2-color)',
              color: 'var(--white-color)'
            }
            : {}
        }
      >
        <p className={classes.text}>{children}</p>
      </div>
    </Tooltip>
  )
}

export default withStyles(styles)(TableTag)
