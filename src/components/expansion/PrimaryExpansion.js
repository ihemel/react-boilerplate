import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import classNames from 'classnames';

const styles = theme => ({
  expanded: {
    boxShadow: '0 1px 5px 0 rgba(0, 0, 0, 0.18)'
    // margin: '15px 0 !important'
  },
  title: {
    fontFamily: 'Apercu Bold',
    fontSize: '1.3rem',
    color: theme.text.primary,
    whiteSpace: 'nowrap',
    width: '10.3125rem',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    paddingRight: '0 !important'
  },
  counter: {
    fontSize: '.9rem',
    fontFamily: 'Apercu',
    color: theme.text.grey,
    paddingRight: '2.5rem !important'
  },
  paper: {
    // boxShadow: 'none',
    // border: `1px solid ${theme.text.greyLight}`,
    boxShadow: '0 1px 5px 0 rgba(0, 0, 0, 0.18)',
    borderTop: `1px solid ${theme.text.greyLight}`
  },
  summaryContent: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    boxShadow: 'none',
    margin: '15px 0 !important'
  },
  expandIcon: {
    boxShadow: 'none'
  }
});

class PrimaryExpansion extends PureComponent {
  handleChange = (event, expanded) => {
    const { toggleExpand } = this.props;
    if (toggleExpand) {
      expanded ? toggleExpand('open') : toggleExpand('close');
    }
  };

  render() {
    const {
      classes,
      title,
      counter,
      className,
      headerclass,
      detailsclass,
      children,
      expanded,
      ...others
    } = this.props;

    return (
      <ExpansionPanel
        className={classNames(classes.paper, className)}
        {...others}
        expanded={expanded}
        onChange={this.handleChange}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon style={{ transform: 'scale(2)' }} />}
          className={headerclass}
          classes={{
            expanded: classes.expanded,
            content: classes.summaryContent,
            expandIcon: classes.expandIcon
          }}
        >
          <Typography className={classes.title}>{title}</Typography>
          {counter > 0 ? (
            <Typography component="p" className={classes.counter}>
              ({counter} Selected!)
            </Typography>
          ) : (
            <></>
          )}
        </ExpansionPanelSummary>

        <ExpansionPanelDetails className={detailsclass}>
          {children}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

PrimaryExpansion.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
  headerclass: PropTypes.string,
  detailsclass: PropTypes.string
};

export default withStyles(styles)(PrimaryExpansion);
