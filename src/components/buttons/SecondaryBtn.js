import React from "react";
import PropTypes from "prop-types";
import classNames from 'classnames';
import withPropsStyles from "../hoc/withPropsStyles";
import Btn from "./Btn";

const styles = (props, theme) => ({
  root: {
    boxShadow: "none",
    border: `2px solid ${theme.text.primary}`,
    fontFamily: "Apercu Black",
    textTransform: "capitalize",
    transition: "all .5s ease-out",
    borderRadius: "11px",
    
    backgroundColor: props.bgcolor ? props.bgcolor : theme.palette.primary.main,
    color: props.txtcolor ? props.txtcolor : theme.text.primary,

    "&:hover": {
      backgroundColor: props.hoverbg  ? props.hoverbg : theme.palette.secondary.main,
      color: props.hovertxt ? props.hovertxt : theme.palette.common.white
    }
  }
});

const SecondaryBtn = props => {
  const { classes, title, className, ...others } = props;

  return <Btn 
    className={classNames(classes.root, className)} 
    title={title} 
    {...others} 
  />;
};

SecondaryBtn.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.any,
  alt: PropTypes.string,
  style: PropTypes.object,
  iconstyle: PropTypes.object,
  bgcolor: PropTypes.string,
  txtcolor: PropTypes.string,
  hoverbg: PropTypes.string,
  hovertxt: PropTypes.string,
};

export default withPropsStyles(styles)(SecondaryBtn);


