import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  root: {
    boxShadow: 'none',
    transition: 'all .5s ease-out',
    color: theme.text.primary,
    border: 0,
    height: "100%",

    '&:hover': {
      background: theme.text.greyLight
    }
  },
});

const BtnIcon = props => {
  const {classes, icon, alt, iconstyle, className, ...others} = props
  
  return (
    <Button className={classNames(classes.root, className)} {...others} >
    <img src={icon} alt={alt} style={iconstyle} />
    </Button>
  );
};



BtnIcon.propTypes = {
  icon: PropTypes.any.isRequired,
  alt:  PropTypes.string,
  style: PropTypes.object,
  iconstyle: PropTypes.object
};


export default withStyles(styles)(BtnIcon);

