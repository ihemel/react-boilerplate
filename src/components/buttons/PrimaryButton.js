import React from 'react'
import Button from '@material-ui/core/Button'
import PropTypes from "prop-types";

const PrimaryButton = props => {
  const { className, title, ...others } = props

  return (
    <Button 
        variant='outlined' 
        className={className}
        {...others}
    >
      {title}
    </Button>
  )
}


PrimaryButton.propTypes = {
  title: PropTypes.string
};

export default PrimaryButton

