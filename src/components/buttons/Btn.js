import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  root: {
    boxShadow: "none",
    border: `1px solid ${theme.text.grey}`,
    padding: ".5rem",
    color: theme.text.primary,
    fontFamily: "Apercu",
    textTransform: "capitalize"
  }
});

const Btn = props => {
  const { classes, title, icon, alt, iconclass, iconstyle, className, ...others } = props;

  return (
    <Button className={classNames(classes.root, className)} {...others}>
      <img src={icon} alt={alt} className={iconclass} style={iconstyle} />
      {title}
    </Button>
  );
};

Btn.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.any,
  alt: PropTypes.string,
  style: PropTypes.object,
  iconstyle: PropTypes.object
};

export default withStyles(styles)(Btn);


