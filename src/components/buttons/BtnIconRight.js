import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import classNames from 'classnames';
import Button from "@material-ui/core/Button";

const styles = theme => ({
  root: {
    boxShadow: "none",
    border: `1px solid ${theme.text.grey}`,
    padding: ".5rem",
    color: theme.text.primary,
    fontFamily: "Apercu",
    textTransform: "capitalize"
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
});

const BtnIconRight = props => {
  const { classes, title, icon, alt, iconstyle, className, ...others } = props;

  return (
    <Button  className={classNames(classes.root, className)} {...others}>
      {title}
      <img src={icon} alt={alt} className={classes.rightIcon} style={iconstyle} />
    </Button>
  );
};

BtnIconRight.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.any,
  alt:  PropTypes.string,
  style: PropTypes.object,
  iconstyle: PropTypes.object
};

export default withStyles(styles)(BtnIconRight);

