import React from "react";
import PropTypes from "prop-types";
import classNames from 'classnames';
import Btn from "./Btn";
import withPropsStyles from "../hoc/withPropsStyles";

const styles = (props, theme) => ({
  root: {
    boxShadow: "0 2px 5px 0 rgba(0, 0, 0, 0.25)",
    // border: `1px solid ${theme.text.grey}`,
    border: 'none',
    padding: ".5rem",
    fontFamily: "Apercu Bold",
    textTransform: "capitalize",
    transition: "all .5s ease-out",
    borderRadius: '8px',

    backgroundColor: props.bgcolor ? props.bgcolor : theme.palette.primary.main,
    color: props.txtcolor ? props.txtcolor : theme.text.primary,

    "&:hover": {
      backgroundColor: props.hoverbg  ? props.hoverbg : theme.palette.secondary.main,
      color: props.hovertxt ? props.hovertxt : theme.palette.common.white
    }
  }
});

const PrimaryBtn = props => {
  const { classes, title, className, ...others } = props;

  return <Btn 
    className={classNames(classes.root, className)}
    title={title} 
    {...others} 
  />;
};

PrimaryBtn.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.any,
  alt: PropTypes.string,
  style: PropTypes.object,
  iconStyle: PropTypes.object,
  bgcolor: PropTypes.string,
  txtcolor: PropTypes.string,
  hoverbg: PropTypes.string,
  hovertxt: PropTypes.string,
};

export default withPropsStyles(styles)(PrimaryBtn);
