import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Avatar, Button } from '@material-ui/core'
import PropTypes from 'prop-types'

const styles = {
  btn: {
    border: 'none',
    borderRadius: '50%',
    padding: '0'
  }
}

const CircleBtn = props => {
  const { classes, className, ...others } = props
  return (
    <Button className={`${classes.btn} ${className}`}>
      <Avatar {...others} />
    </Button>
  )
}

CircleBtn.propTypes = {
  src: PropTypes.any,
  alt: PropTypes.string
}

export default withStyles(styles)(CircleBtn)
