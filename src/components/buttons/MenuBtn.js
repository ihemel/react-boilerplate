import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { Button } from '@material-ui/core'

const styles = {
  btn: {
    border: 0,
    borderRadius: 0,
    width: '100%',
    height: '3.5rem',
    fontFamily: 'apercu bold',
    textTransform: 'capitalize',
    fontSize: '1.1rem',
    lineHeight: '1',

    '&:not(:last-child)': {
      borderRight: '1px solid var(--grey2-color)'
    },

    '&:hover': {
      transition: 'all .5s',
      backgroundColor: 'var(--primary-color)'
    }
  }
}

const MenuBtn = props => {
  const {
    classes,
    title,
    icon,
    iconClass,
    alt,
    spanTxt,
    spanClass,
    ...others
  } = props
  return (
    <Button className={classes.btn} {...others}>
      {spanTxt && <span className={spanClass}>{spanTxt}</span>}
      <img className={iconClass} src={icon} alt={alt} />
      <span>{title}</span>
    </Button>
  )
}

MenuBtn.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.any,
  alt: PropTypes.string,
  iconClass: PropTypes.string,
  spanTxt: PropTypes.string
}

export default withStyles(styles)(MenuBtn)
