import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select'

const styles = {
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: 'rgba(0, 0, 0, 0.23)',
      border: '1px solid'
    },
  },
  notchedOutline: {},
}

const PrimarySelect = props => {
  const {
    classes,
    items,
    placeholder,
    fullWidth,
    classNameForm,
    icon,
    error,
    helperText,
    ...others
  } = props

  return (
    <FormControl
      variant='outlined'
      fullWidth={fullWidth}
      className={classNameForm}
      error = {false || error}
    >
      <Select
        input={
          <OutlinedInput
            labelWidth={0}
            classes = {{
              root: classes.cssOutlinedInput,
              focused: classes.cssFocused,
              notchedOutline: classes.notchedOutline,
            }}
            className={classes.select}
          />
        }
        {...others}
      >
        <MenuItem disabled value='none'>
          <span style={{color: '#4a4a4a'}}>{placeholder}</span>
        </MenuItem>

        {items.map((item, index) => (
          <MenuItem key={index} value={item.value} disabled={!!item.disabled}>
            {item.name}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>{helperText}</FormHelperText>
    </FormControl>
  )
}

export default withStyles(styles)(PrimarySelect)


