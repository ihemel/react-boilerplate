import React from 'react'
import PropTypes from 'prop-types'
import { Avatar, Menu, MenuItem } from '@material-ui/core'

class User extends React.Component {
  state = {
    anchorEl: null
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  render () {
    const { anchorEl } = this.state
    const { className, src, alt, isAuth, login, logout } = this.props

    return (
      <div style={{ cursor: 'pointer' }}>
        <Avatar
          className={className}
          onClick={this.handleClick}
          src={src}
          alt={alt}
        />
        <Menu
          id='simple-menu'
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleClose}>Profile</MenuItem>
          <MenuItem onClick={this.handleClose}>My account</MenuItem>
          {isAuth ? 
            <MenuItem onClick={logout}>Logout</MenuItem> :
            <MenuItem onClick={login}>Login</MenuItem>
          }
        </Menu>
      </div>
    )
  }
}

User.propTypes = {
  src: PropTypes.any,
  alt: PropTypes.string
}

export default User
