import React from "react";
import Avatar from "@material-ui/core/Avatar";
import PropTypes from "prop-types";

const Logo = props => {
  const { ...others } = props;
  return <Avatar {...others} />;
};

Logo.propTypes = {
  src: PropTypes.any,
  alt: PropTypes.string
};

export default Logo;
