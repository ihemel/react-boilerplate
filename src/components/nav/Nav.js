import React from 'react'
import PropTypes from 'prop-types'
import classNames from "classnames";
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'

const styles = theme => ({
  navbar: {
    // boxShadow: `0 0 4px 0 ${theme.text.black}`,
    boxShadow: '0 0 3px 1px rgba(0, 0, 0, 0.25)',
    // borderBottom: `solid 1px ${theme.text.greyLight}`,
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    minHeight: '30px',
    padding: 0
  }
})

const Nav = props => {
  const { classes, appbarclass, className, children} = props

  return (
      <AppBar 
        position='static' 
        color='inherit' 
        className={
          classNames(classes.navbar, appbarclass)
        }
      >
        <Toolbar 
          className={
            classNames(classes.toolbar, className)
          }
          >
          {children}
        </Toolbar>
      </AppBar>
  )
}

Nav.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Nav)
