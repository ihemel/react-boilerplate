import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#ffe33f",
      light: "#fffcf4",
      white: "#ffffff"
    },
    secondary: {
      main: "#e85d75"
    }
  },
  typography: {
    useNextVariants: true,
    fontFamily: ["Apercu", "sans-serif"].join(","),
    // htmlFontSize: 10,
    h4: {
      color: "#4a4a4a",
      fontSize: "1rem",
      fontFamily: "Apercu"
    },
    h5: {
      color: "#4a4a4a",
      fontSize: "1.2rem",
      fontFamily: "Apercu Bold"
    },
    subtitle1: {
      color: "#4a4a4a",
      fontSize: ".8rem",
      fontFamily: "Apercu Bold Italic"
    },
    h6: {
      color: "#4a4a4a",
      fontSize: "1rem",
      fontFamily: "Apercu Black"
    }
  },
  text: {
    primary: '#4a4a4a',
    grey: '#979797',
    greyLight: '#e7e7e7',
    greyLight2: '#ececec',
    red: '#e63946',
    black: 'rgba(0,0,0,.5)',
    green: '#e6ffdd',
    greenLight: '#f3fcf0',
    greenDark: '#dce1da'
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: 3,
        border: "1px solid #979797",
        boxShadow: "none",
        color: "#4a4a4a"
      }
    },

    MuiFormLabel: {
      root: {
        color: "#4a4a4a",
        fontFamily: "Apercu Light",

        "&$focused": {
          color: "#4a4a4a",
          fontFamily: "Apercu Bold"
        }
      }
    },

    MuiOutlinedInput: {
      input: {
        fontFamily: "Apercu",
        color: "#4a4a4a"
      }
    },

    MuiNotchedOutline: {
      "&$focused": {
        "&$focused": {
          border: "1px solid #979797"
        }
      },
      error: {
        border: "1px solid red",
        "&$focused": {
          border: "1px solid red"
        }
      }
    }
  }
});

export default theme;
