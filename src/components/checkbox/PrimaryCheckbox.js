import React from "react";
import withPropsStyles from "../hoc/withPropsStyles";
import PropTypes from "prop-types";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import classNames from "classnames";

const styles = (props, theme) => ({
  root: {
    color: theme.palette.primary.main,
    "&$checked": {
      color: props.checkboxColor || theme.text.primary
    }
  },
});

const PrimaryCheckbox = props => {
  const { classes, title, checkboxClass, labelClass, ...others } = props;
  return (
    <div>
      <FormControlLabel
        control={
          <Checkbox
            className={
              classNames(
                classes.root, 
                classes.checked, 
                checkboxClass
              )
            }
            {...others}
          />
        }
        label={title}
        className={labelClass}
      />
    </div>
  );
};

PrimaryCheckbox.propTypes = {
  title: PropTypes.string,
  checkboxClass: PropTypes.string,
  labelClass: PropTypes.string,
  style: PropTypes.object,
  checkboxColor: PropTypes.string
};

export default withPropsStyles(styles)(PrimaryCheckbox);


