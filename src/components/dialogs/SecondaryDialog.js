import React from 'react'
import Dialog from '@material-ui/core/Dialog'

import { withStyles } from '@material-ui/core/styles'

const styles = {
  paper: {
    width: '95%',
    margin: 0,
    borderRadius: "10px",
  }
}

const SecondaryDialog = props => {
  const { children, classes, ...others } = props
  return (
    <Dialog
    classes={{paper: classes.paper }}
     {...others}>
      {children}
    </Dialog>
  )
}

export default withStyles(styles)(SecondaryDialog)
