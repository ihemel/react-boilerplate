import React from 'react'
import Dialog from '@material-ui/core/Dialog'

const PrimaryDialog = props => {
  const { children, ...others } = props
  return (
    <Dialog {...others}>
      {children}
    </Dialog>
  )
}

export default PrimaryDialog

