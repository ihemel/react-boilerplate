import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import Slide from '@material-ui/core/Slide'

function Transition (props) {
  return <Slide direction='up' {...props} />
}

const styles = {
  root: {
    alignItems: 'flex-end'
  },
  paper: {
    margin: 0,
    alignSelf: 'flex-end',
    width: '100%',
    maxWidth: '100%',
    borderRadius: '0',
    borderTopLeftRadius: '15px',
    borderTopRightRadius: '15px',
  },
  scrollPaper: {
    alignItems: 'flex-end'
  }
}

const BottomDialog = props => {
  const { classes, children, ...others } = props

  return (
    <Dialog
      classes={{ root: classes.root, paper: classes.paper }}
      TransitionComponent={Transition}
      {...others}
    >
      {children}

    </Dialog>
  )
}

export default withStyles(styles)(BottomDialog)
