import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import Zoom from '@material-ui/core/Zoom';

function Transition (props) {
  return <Zoom {...props} />
}

const styles = theme => ({
  root: {
    alignItems: 'flex-end'
  },
  paper: {
    margin: 0,
    width: '100%',
    height: '100%',
    maxWidth: '100%',
    maxHeight: '100%',
    borderRadius: '0'
  }
})

const FullScreenDialog = props => {
  const { classes, open, handleClose, children, ...others } = props

  return (
    <Dialog
      classes={{ root: classes.root, paper: classes.paper }}
      open={open}
      TransitionComponent={Transition}
      onClose={handleClose}
      aria-labelledby='form-dialog-title'
      {...others}
    >
      {children}

    </Dialog>
  )
}

FullScreenDialog.defaultProps = {
  open: true,
}

FullScreenDialog.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func.isRequired,
}

export default withStyles(styles)(FullScreenDialog)
