import React from 'react'

import Navbar from '../feature/navbar/'
import Menubar from '../feature/menubar/'

const Profile = props => {
  return (
    <div>
      <Navbar />
      <Menubar />
      <h1>Profile</h1>
    </div>
  )
}

export default Profile
