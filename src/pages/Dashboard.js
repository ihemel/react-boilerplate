import React from 'react'

import Navbar from '../feature/navbar/'
import Menubar from '../feature/menubar/'

const Dashboard = props => {
  return (
    <div>
      <Navbar />
      <Menubar />
      <h1>welcome Dashboard</h1>
    </div>
  )
}

export default Dashboard
