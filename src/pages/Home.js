import React from 'react'

import Navbar from '../feature/navbar/'
import Menubar from '../feature/menubar/'

const Home = props => {
  return (
    <div>
      <Navbar />
      <Menubar />
      <h1>Home</h1>
    </div>
  )
}

export default Home
