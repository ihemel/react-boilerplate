import React from 'react'

/* ===============  Import Single Component  ================ */
import { storiesOf } from '@storybook/react'
import { MuiThemeProvider } from '@material-ui/core/styles'
import {
  Btn,
  BtnIcon,
  BtnIconRight,
  PrimaryBtn,
  Logo,
  Nav,
  SecondaryBtn,
  PrimaryInput,
  SecondaryInput,
  PrimaryExpansion,
  FullScreenDialog,
  SecondaryDialog,
  BottomDialog,
  PrimaryCheckbox,
  PrimarySelect,
  DragDropFileInput,
  PrimaryPopover,
  PrimaryDialog,
  theme,
  PrimaryAutocomplete,
  User,
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableCard,
  TableText,
  TableTag
} from '../components'

/* ==========================================
                    BUTTON
  ============================================= */
storiesOf('Button', module)
  .add('Button', () => (
    <MuiThemeProvider theme={theme}>
      <Btn
        title='Button'
        color='primary'
        variant='contained'
        // icon={fbIcon}
        alt='Fb Icon'
        style={{
          fontSize: '1.2rem'
        }}
        iconstyle={{
          width: '1.5rem',
          margin: '0 .5rem'
        }}
        className='test'
      />

      <Btn
        disabled
        title='Disabled Button'
        variant='contained'
        color='secondary'
        style={{
          background: theme.palette.common.greyLight
        }}
      />
    </MuiThemeProvider>
  ))
  .add('Button Icon Right', () => (
    <MuiThemeProvider theme={theme}>
      <BtnIconRight
        title='Button'
        color='primary'
        variant='contained'
        // icon={fbIcon}
        alt='Fb Icon'
        style={{
          fontSize: '1.2rem'
        }}
        iconstyle={{
          width: '1.2rem',
          margin: '0 .5rem'
        }}
        className='test'
      />

      <BtnIconRight
        title='Add Category'
        bgcolor='orange'
        txtcolor='black'
        hoverbg='green'
        hovertxt='orange'
        // icon={fbIcon}
        alt={'Dropdown Icon'}
        style={{
          padding: '1rem',
          display: 'flex',
          justifyContent: 'space-between',
          margin: '1rem'
        }}
        iconstyle={{
          width: '1.2rem',
          margin: '0 .5rem'
        }}
      />
    </MuiThemeProvider>
  ))
  .add('Primary Button', () => (
    <MuiThemeProvider theme={theme}>
      <PrimaryBtn
        title='Primary Button'
        bgcolor='orange'
        txtcolor='white'
        hoverbg='green'
        hovertxt='orange'
        // icon={fbIcon}
        alt={'Fb Icon'}
        style={{
          width: '12rem',
          padding: '1rem',
          display: 'flex',
          justifyContent: 'space-between',
          margin: '1rem'
        }}
        iconstyle={{
          width: '1.5rem',
          margin: '0 .5rem'
        }}
        className='test'
        iconclass='iconClass'
      />

      <PrimaryBtn
        title='Our Services'
        style={{
          width: '12rem',
          padding: '.5rem',
          margin: '1rem'
        }}
      />

      <PrimaryBtn title='Save' />

      <PrimaryBtn
        title='Delete'
        bgcolor={theme.palette.common.white}
        hoverbg={theme.text.greyLight}
        hovertxt={theme.text.primary}
      />

      <PrimaryBtn
        title='Cancel'
        bgcolor={theme.palette.common.white}
        hoverbg={theme.text.greyLight}
        hovertxt={theme.text.primary}
        style={{
          border: 'none'
        }}
      />

      <PrimaryBtn
        title='Preview'
        bgcolor={theme.palette.common.white}
        txtcolor={theme.text.red}
        hoverbg={theme.text.greyLight}
        hovertxt={theme.text.red}
      />

    </MuiThemeProvider>
  ))
  .add('Secondary Button', () => (
    <MuiThemeProvider theme={theme}>
      <SecondaryBtn
        title='Secondary Button'
        bgcolor='orange'
        txtcolor='white'
        hoverbg='green'
        hovertxt='orange'
        // icon={fbIcon}
        alt={'Fb Icon'}
        style={{
          width: '13rem',
          padding: '1rem',
          display: 'flex',
          justifyContent: 'space-between',
          margin: '1rem'
        }}
        iconstyle={{
          width: '1.5rem',
          margin: '0 .5rem'
        }}
        className='test'
      />

      <SecondaryBtn title='Next' />

      <SecondaryBtn
        title='Back'
        bgcolor={theme.palette.common.white}
        txtcolor={theme.text.primary}
        hoverbg={theme.text.greyLight}
        hovertxt={theme.text.primary}
      />

      <SecondaryBtn
        title='Done'
        style={{
          width: '12rem',
          padding: '.5rem',
          margin: '1rem'
        }}
      />

      <SecondaryBtn
        title='Done'
        // icon={happyIcon}
        alt='Happy Icon'
        style={{
          width: '12rem',
          padding: '.5rem',
          margin: '1rem'
        }}
        iconstyle={{
          margin: '0 .5rem'
        }}
      />

      
    </MuiThemeProvider>
  ))
  

/* ==========================================
                    INPUT
  ============================================= */
storiesOf('Input', module)
  .add('Primary Input', () => (
    <MuiThemeProvider theme={theme}>
      <PrimaryInput
        label='Primary Input'
        type='email'
        name='email'
        autoComplete='email'
        placeholder='Max Length 5'
        margin='dense'
        fullWidth
        className='test'
        InputProps={{ inputProps: { maxLength: 5 } }}
      />

      <PrimaryInput
        placeholder='Error Input'
        autoComplete='email'
        error
        margin='normal'
      />

      <PrimaryInput placeholder='Product Name' required margin='normal' />

      <PrimaryInput
        placeholder='Price (TK)'
        type='number'
        required
        margin='dense'
      />

      <PrimaryInput
        placeholder='Product Description'
        multiline
        rows={4}
        rowsMax={5}
        margin='normal'
      />
    </MuiThemeProvider>
  ))
  .add('Secondary Input', () => (
    <MuiThemeProvider theme={theme}>
      <SecondaryInput />
    </MuiThemeProvider>
  ))



/* ==========================================
                    IMAGES
  ============================================= */

storiesOf('Images', module).add('Logo', () => (
  <MuiThemeProvider theme={theme}>
    <Logo
      className='test'
      style={{
        width: '5rem',
        height: '5rem'
      }}
      // src={LogoIcon}
      alt='SH Logo'
    />
  </MuiThemeProvider>
))

storiesOf('Images', module).add('User', () => (
  <MuiThemeProvider theme={theme}>
    <User
      style={{
        width: '5rem',
        height: '5rem'
      }}
      // src={user}
      alt='SH Logo'
    />
  </MuiThemeProvider>
))

/* ==========================================
                    POPOVER
  ============================================= */
storiesOf('Popover', module)
  .add('Primary Popover', () => (
    <MuiThemeProvider theme={theme}>
      <PrimaryPopover title='?' className='test'>
        <div className='popover_class'>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maxime earum
          vel iusto dignissimos error ipsa asperiores aspernatur architecto unde
          ea ipsam nisi perferendis quam, non inventore ut iure consequatur
          minima.
        </div>
      </PrimaryPopover>
    </MuiThemeProvider>
  ))

/* ==========================================
                  NAVIGATION
  ============================================= */
storiesOf('Nav', module)
  .add('Nav', () => (
    <MuiThemeProvider theme={theme}>
      <Nav />
    </MuiThemeProvider>
  ))

/* ==========================================
                Expansion Panel
  ============================================= */
storiesOf('Expansion Panel', module).add('Primary Expansion', () => (
  <MuiThemeProvider theme={theme}>
    <PrimaryExpansion
      title='Album 1'
      className='test'
      headerclass='header class'
      detailsclass='details class'
    >
      <p>I'm childen</p>
    </PrimaryExpansion>
  </MuiThemeProvider>
))

/* ==========================================
                Dialogs
  ============================================= */
storiesOf('Dialogs popups', module)
  .add('Full Screen Dialog', () => (
    <MuiThemeProvider theme={theme}>
      <FullScreenDialog open onClose={() => console.log('dialog')}>
        <div>I am childen</div>
      </FullScreenDialog>
    </MuiThemeProvider>
  ))
  .add('Bottom Dialog', () => (
    <MuiThemeProvider theme={theme}>
      <BottomDialog open onClose={() => console.log('dialog')}>
        <div>I am childen</div>
      </BottomDialog>
    </MuiThemeProvider>
  ))
  .add('SecondaryDialog Dialog', () => (
    <MuiThemeProvider theme={theme}>
      <SecondaryDialog open onClose={() => console.log('dialog')} />
    </MuiThemeProvider>
  ))

/* ==========================================
                Checkbox
============================================= */
storiesOf('Checkbox', module)
  .add('Primary Checkbox', () => (
    <MuiThemeProvider theme={theme}>
      <PrimaryCheckbox
        title='Primary Checkbox'
        checkboxclass='test-checkbox'
        labelclass='test-label'
      />
    </MuiThemeProvider>
  ))


/* ==========================================
                    Selects
  ============================================= */
// 'Choose Type', 'Color/Type', 'Size'
storiesOf('Selects', module)
  .add('Primary Select', () => (
    <MuiThemeProvider theme={theme}>
      <PrimarySelect
        // fullWidth
        placeholder='Choose Type'
        items={[
          {
            name: 'Color/Type',
            value: 'color'
          },
          {
            name: 'Size',
            value: 'size'
          }
        ]}
      />
    </MuiThemeProvider>
  ))
