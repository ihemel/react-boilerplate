/**
 *  Redux sagas class init
 */
import { takeLatest, all } from 'redux-saga/effects'
import * as types from './Types'

import {LoginSaga, LogoutSaga} from '../feature/auth/saga'


export default function * watch () {
  yield all([takeLatest(types.LOGIN_REQUEST, LoginSaga)])
  yield all([takeLatest(types.LOGOUT_REQUEST, LogoutSaga)])
}
