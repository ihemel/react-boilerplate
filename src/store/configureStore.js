import { createStore, compose, applyMiddleware } from 'redux'
import { persistStore, persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/es/storage' // default: localStorage if web, AsyncStorage if react-native
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import { routerMiddleware } from 'connected-react-router';
import { connectRouter } from 'connected-react-router';
import { history } from '../routes/Approuter';


import rootReducer from './Reducers' // where reducers are a object of reducers
import sagas from './Sagas' // where sagas are a object of sagas


let key = window.location.hostname.match("^([^.]+)")[0]

const config = {
  key: key,
  storage,
  whitelist: [
    'Auth',
    'router'
  ],
  debug: true
}

const middleware = []
const sagaMiddleware = createSagaMiddleware()

middleware.push(sagaMiddleware)
middleware.push(createLogger())
middleware.push(routerMiddleware(history))

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

// create reducers persist config and rootreducer
const reducers = (history) => persistCombineReducers(config, {...rootReducer, router: connectRouter(history)})

// persist config
const enhancers = [applyMiddleware(...middleware)]
// const initialState = {};
const persistConfig = { enhancers }

// create store with root reducer and enhancers
const store = createStore(reducers(history), undefined, composeEnhancers(...enhancers))

// persistor callback with store and persist config
const persistor = persistStore(store, persistConfig, () => {
  console.log('Rehydration complete', store.getState())
})


// this is configureStore object
const configureStore = () => {
  return { persistor, store }
}

// run feature sagas through sagaMiddleWare - its a function
sagaMiddleware.run(sagas)

export default configureStore

