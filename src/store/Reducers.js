import * as NotificationReducer from '../feature/notification/Reducer'
import * as Auth from '../feature/auth/Reducer'


export default Object.assign(NotificationReducer, Auth)
